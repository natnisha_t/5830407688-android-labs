package th.ac.kku.tieosuwan.natnisha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        textView = (TextView) findViewById(R.id.green);
        textView.setTextSize(30);
        textView.setText("green");
        textView.setGravity(Gravity.CENTER);
    }
}

