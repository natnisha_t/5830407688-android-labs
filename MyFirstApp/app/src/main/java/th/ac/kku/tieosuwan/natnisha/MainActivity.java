package th.ac.kku.tieosuwan.natnisha;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private String name;
    private String phone;
    public static final String NAME_KEY = "name";
    public static final String PHONE_KEY = "phone";
    private EditText edit_textName;
    private EditText edit_textPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_textName = (EditText) findViewById(R.id.edit_textName);
        edit_textPhone = (EditText) findViewById(R.id.edit_textPhone);

        Button button = (Button) findViewById(R.id.submit_bt);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = edit_textName.getText().toString();
                phone = edit_textPhone.getText().toString();
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra(PHONE_KEY, phone);
                intent.putExtra(NAME_KEY, name);
                startActivity(intent);
            }
        });
    }
}
