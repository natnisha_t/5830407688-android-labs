package th.ac.kku.tieosuwan.natnisha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private String name;
    private TextView tv_name;
    private String phone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        name = this.getIntent().getStringExtra(MainActivity.NAME_KEY);
        phone = this.getIntent().getStringExtra(MainActivity.PHONE_KEY);
        tv_name = (TextView) findViewById(R.id.tv_second_name);
        tv_name.setText(name + " " + getText(R.string.eng) + " " + phone);
        Log.d("Test", name);
        Log.d("Test", phone);
    }
}
