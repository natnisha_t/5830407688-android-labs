package th.ac.kku.tieosuwan.natnisha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edit1, edit2;
    private RadioGroup radio;
    private Button enter;
    private float var1, var2, ans;
    private TextView tv_ans;
    private Switch sw;
    private ProgressBar progress;
    private long timeStart, timeEnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit1 = (EditText) findViewById(R.id.edit1);
        edit2 = (EditText) findViewById(R.id.edit2);
        enter = (Button) findViewById(R.id.enter);
        radio = (RadioGroup) findViewById(R.id.radio);
        tv_ans = (TextView) findViewById(R.id.tv_ans);
        sw = (Switch) findViewById(R.id.sw);
        progress = (ProgressBar) findViewById(R.id.progress);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        showToast("width= " + width + " height= " + height);

        progress.setIndeterminate(true);
        sw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sw.isChecked()) {
                    sw.setText("ON  ");
                }
                else {
                    sw.setText("OFF ");
                }
            }
        });
        radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                calculate(i);
            }
        });
    }
    private void calculate(int i) {
        timeStart= System.currentTimeMillis();
            acceptNumber();
            if(i == R.id.add) {
                ans = var1+var2;
            }
            else if(i == R.id.minus) {
                ans = var1-var2;
            }
            else if(i == R.id.divide) {
                if (var2 == 0) {
                    showToast("Please divide by a non-zero number");
                } else {ans = var1/var2;}
            }
            else {
                ans = var1*var2;
            }

            tv_ans.setText(String.valueOf(ans));
            timeEnd = System.currentTimeMillis();
            Log.d("Calculation:", "computation time = "+String.valueOf((timeEnd - timeStart)/1000.0));
    }
    private void acceptNumber() {
        try {
            var1 = Float.parseFloat(edit1.getText().toString());
            var2 = Float.parseFloat(edit2.getText().toString());
        } catch (NumberFormatException e) {
            showToast( "Please enter only a number.");
        }
    }
    private void showToast(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
    public void buttonClick (View v) {
        calculate(radio.getCheckedRadioButtonId());
    }
}
